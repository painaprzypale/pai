create table department (
	"id"    bigserial primary key,
	"region" varchar(20) not null,
    "web_address" varchar(100) not null
);
