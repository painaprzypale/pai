create table parcel (
	original_department_id bigint not null,
	id bigserial not null,
    receiver_address varchar not null,
    weight double precision not null,
    last_modification_date int,
    sync boolean default false,
    primary key(original_department_id, id)
)