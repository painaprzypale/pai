create table action_entry (
	id bigserial primary key,
	postman_id bigint not null,
    parcel_id bigint not null,
    department_id int not null,
    action_no int not null
);