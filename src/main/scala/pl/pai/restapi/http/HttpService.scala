package pl.pai.restapi.http

import akka.http.scaladsl.server.Directives._
import pl.pai.restapi.http.routes._
import pl.pai.restapi.services._
import pl.pai.restapi.utils.CorsSupport

import scala.concurrent.ExecutionContext

class HttpService(usersService: PostmenService,
                  authService: AuthService,
                  actionEntryService: ActionEntryService,
                  departmentService: DepartmentService,
                  parcelService: ParcelService
                 )(implicit executionContext: ExecutionContext) extends CorsSupport {

  val usersRouter = new PostmenServiceRoute(authService, usersService)
  val authRouter = new AuthServiceRoute(authService)
  val actionEntryRouter = new ActionEntryRoute(authService, actionEntryService)
  val departmentRouter = new DepartmentRoute(authService, departmentService)
  var parcelRouter = new ParcelRoute(authService, parcelService)

  val routes =
    pathPrefix("v1") {
      corsHandler {
        usersRouter.route ~
        authRouter.route ~
        actionEntryRouter.route ~
        departmentRouter.route ~
        parcelRouter.route
      }
    }

}
