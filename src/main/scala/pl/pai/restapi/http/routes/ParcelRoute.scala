package pl.pai.restapi.http.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.PathMatchers.LongNumber
import de.heikoseeberger.akkahttpcirce.CirceSupport
import io.circe.generic.auto._
import io.circe.syntax._
import pl.pai.restapi.http.SecurityDirectives
import pl.pai.restapi.models.{ParcelEntity, ParcelEntityUpdate}
import pl.pai.restapi.services.{AuthService, ParcelService}

import scala.concurrent.ExecutionContext

class ParcelRoute(val authService: AuthService, parcelService: ParcelService)(implicit executionContext: ExecutionContext) extends CirceSupport with SecurityDirectives {
  import StatusCodes._
  import parcelService._

  val route = pathPrefix("parcel") {
    pathEndOrSingleSlash {
      get {
        complete(getParcel().map(_.asJson))
      } ~
        post {
          entity(as[ParcelEntity]) { parcel =>
            complete(createParcel(parcel))
          }
        }
    } ~
      pathPrefix("sync") {
        pathEndOrSingleSlash {
          get {
            complete(getParcelToSync().map(_.asJson))
          }
        }
      } ~
      pathPrefix(LongNumber) { id =>
        pathEndOrSingleSlash {
          get {
            complete(getParcelById(id).map(_.asJson))
          } ~
            post {
              entity(as[ParcelEntityUpdate]) { parcelUpdate =>
                complete(updateParcel(id, parcelUpdate).map(_.asJson))
              }
            }

        } ~
          pathPrefix(LongNumber) { department =>
            get {
              complete(getParcelByIdAndDepId(id, department).map(_.asJson))
            } ~
              post {
                entity(as[ParcelEntityUpdate]) { parcelUpdate =>
                  complete(updateParcelByIdAndDep(id, department, parcelUpdate).map(_.asJson))
                }
              }
          }
      }
  }
}

