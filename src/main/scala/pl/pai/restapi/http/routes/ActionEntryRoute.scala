package pl.pai.restapi.http.routes

import de.heikoseeberger.akkahttpcirce.CirceSupport
import pl.pai.restapi.http.SecurityDirectives
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.PathMatchers.LongNumber
import pl.pai.restapi.services.{ActionEntryService, AuthService}
import io.circe.generic.auto._
import io.circe.syntax._
import pl.pai.restapi.models.ActionEntryEntity

import scala.concurrent.ExecutionContext

class ActionEntryRoute(val authService: AuthService, actionEntryService: ActionEntryService)(implicit executionContext: ExecutionContext) extends CirceSupport with SecurityDirectives {
  import StatusCodes._
  import actionEntryService._

  val route = pathPrefix("action_entry") {
    pathEndOrSingleSlash {
      get {
        complete(getActionEntries().map(_.asJson))
      } ~
        post {
          entity(as[ActionEntryEntity]) { actionEntry =>
            onComplete(createActionEntry(actionEntry)) { ignored =>
              complete(Success)
            }
          }
        }
    } ~
      pathPrefix(LongNumber) { id =>
        pathEndOrSingleSlash {
          get {
            complete(getActionEntryById(id).map(_.asJson))
          } ~
          delete {
            onSuccess(deleteActionEntry(id)) { ignored =>
              complete(NoContent)
            }
          }

        }
      } ~
      pathPrefix("of") {
        pathPrefix(LongNumber) { parcel_id =>
          pathEndOrSingleSlash {
            get {
              complete(getLastActionEntryByParcel(parcel_id).map(_.asJson))
            }
          }
        }
      }
  }
}
