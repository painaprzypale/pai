package pl.pai.restapi.http.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.PathMatchers.IntNumber
import de.heikoseeberger.akkahttpcirce.CirceSupport
import io.circe.generic.auto._
import io.circe.syntax._
import pl.pai.restapi.http.SecurityDirectives
import pl.pai.restapi.models.{DepartmentEntity, DepartmentEntityUpdate}
import pl.pai.restapi.services.{AuthService, DepartmentService}

import scala.concurrent.ExecutionContext

class DepartmentRoute(val authService: AuthService, departmentService: DepartmentService)(implicit executionContext: ExecutionContext) extends CirceSupport with SecurityDirectives {
  import StatusCodes._
  import departmentService._

  val route = pathPrefix("department") {
    pathEndOrSingleSlash {
      get {
        complete(getDepartments().map(_.asJson))
      } ~
        post {
          entity(as[DepartmentEntity]) { department =>
            onComplete(createDepartment(department)) { _ =>
              complete(Success)
            }
          }
        }
    } ~
      pathPrefix(IntNumber) { id =>
        pathEndOrSingleSlash {
          get {
            complete(getDepartmentById(id).map(_.asJson))
          } ~
            post {
              entity(as[DepartmentEntityUpdate]) { departmentUpdate =>
                complete(updateDepartment(id, departmentUpdate).map(_.asJson))
              }
            }

        }
      }
  }
}

