package pl.pai.restapi.services

import pl.pai.restapi.models.{DepartmentEntity, DepartmentEntityUpdate}
import pl.pai.restapi.models.db.DepartmentEntityTable
import pl.pai.restapi.utils.DatabaseService

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by olek on 18.06.2017.
  */
class DepartmentService(val databaseService: DatabaseService)(implicit executionContext: ExecutionContext) extends DepartmentEntityTable {

  import databaseService._
  import databaseService.driver.api._

  def getDepartments(): Future[Seq[DepartmentEntity]] = db.run(departments.result)

  def getDepartmentById(id: Long): Future[Option[DepartmentEntity]] = db.run(departments.filter(_.id === id).result.headOption)
  
  def createDepartment(department: DepartmentEntity): Future[DepartmentEntity] = db.run(departments returning departments += department)

  def updateDepartment(id: Long, departmentUpdate: DepartmentEntityUpdate): Future[Option[DepartmentEntity]] = getDepartmentById(id).flatMap {
    case Some(department) =>
      val updatedDepartment = departmentUpdate.merge(department)
      db.run(departments.filter(_.id === id).update(updatedDepartment)).map(_ => Some(updatedDepartment))
    case None => Future.successful(None)
  }
}
