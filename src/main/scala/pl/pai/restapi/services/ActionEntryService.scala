package pl.pai.restapi.services

import pl.pai.restapi.models.ActionEntryEntity
import pl.pai.restapi.models.db.ActionEntryEntityTable
import pl.pai.restapi.utils.DatabaseService

import scala.concurrent.{ExecutionContext, Future}

class ActionEntryService(val databaseService: DatabaseService)(implicit executionContext: ExecutionContext) extends ActionEntryEntityTable  {
  import databaseService._
  import databaseService.driver.api._

  def getActionEntries(): Future[Seq[ActionEntryEntity]] = db.run(action_entries.result)

  def getActionEntryById(id: Long): Future[Option[ActionEntryEntity]] = db.run(action_entries.filter(_.id === id).result.headOption)

  def getLastActionEntryByParcel(parcel_id : Long): Future[Option[ActionEntryEntity]] =
    db.run(action_entries.filter(_.parcel_id === parcel_id).sortBy(_.id.desc).result.headOption)
  
  def createActionEntry(action_entry: ActionEntryEntity): Future[ActionEntryEntity] = db.run(action_entries returning action_entries += action_entry)

  def deleteActionEntry(id: Long): Future[Int] = db.run(action_entries.filter(_.id === id).delete)

}
