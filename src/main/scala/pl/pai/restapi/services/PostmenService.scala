package pl.pai.restapi.services

import pl.pai.restapi.models.db.PostmanEntityTable
import pl.pai.restapi.models.{PostmanEntity, PostmanEntityUpdate}
import pl.pai.restapi.utils.DatabaseService

import scala.concurrent.{ExecutionContext, Future}

class PostmenService(val databaseService: DatabaseService)(implicit executionContext: ExecutionContext) extends PostmanEntityTable {

  import databaseService._
  import databaseService.driver.api._

  def getUsers(): Future[Seq[PostmanEntity]] = db.run(users.result)

  def getUserById(id: Long): Future[Option[PostmanEntity]] = db.run(users.filter(_.id === id).result.headOption)

  def getUserByLogin(login: String): Future[Option[PostmanEntity]] = db.run(users.filter(_.username === login).result.headOption)

  def createUser(user: PostmanEntity): Future[PostmanEntity] = db.run(users returning users += user)

  def updateUser(id: Long, userUpdate: PostmanEntityUpdate): Future[Option[PostmanEntity]] = getUserById(id).flatMap {
    case Some(user) =>
      val updatedUser = userUpdate.merge(user)
      db.run(users.filter(_.id === id).update(updatedUser)).map(_ => Some(updatedUser))
    case None => Future.successful(None)
  }

  def deleteUser(id: Long): Future[Int] = db.run(users.filter(_.id === id).delete)

}