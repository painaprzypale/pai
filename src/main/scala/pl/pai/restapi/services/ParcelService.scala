package pl.pai.restapi.services

import pl.pai.restapi.models.{ParcelEntity, ParcelEntityUpdate}
import pl.pai.restapi.models.db.ParcelEntityTable
import pl.pai.restapi.utils.DatabaseService

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by olek on 18.06.2017.
  */
class ParcelService(val databaseService: DatabaseService)(implicit executionContext: ExecutionContext) extends ParcelEntityTable {

  import databaseService._
  import databaseService.driver.api._

  def getParcel(): Future[Seq[ParcelEntity]] = db.run(parcel_query.result)

  def getParcelByIdAndDepId(id: Long, department: Long): Future[Option[ParcelEntity]] = db.run(parcel_query.filter(_.id === id).filter(_.original_department_id === department).result.headOption)
  def getParcelToSync(): Future[Seq[ParcelEntity]] = db.run(parcel_query.filter(_.sync === false).result)

  def getParcelById(id: Long): Future[Option[ParcelEntity]] = db.run(parcel_query.filter(_.id === id).result.headOption)

  def createParcel(parcel: ParcelEntity): Future[ParcelEntity] = db.run(parcel_query returning parcel_query += parcel)

  def updateParcel(id: Long, parcelUpdate: ParcelEntityUpdate): Future[Option[ParcelEntity]] = getParcelById(id).flatMap {
    case Some(parcel) =>
      val updatedParcel = parcelUpdate.merge(parcel)
      db.run(parcel_query.filter(_.id === id).update(updatedParcel)).map(_ => Some(updatedParcel))
    case None => Future.successful(None)
  }

  def updateParcelByIdAndDep(id: Long, department: Long, parcelUpdate: ParcelEntityUpdate): Future[Option[ParcelEntity]] = getParcelByIdAndDepId(id, department).flatMap {
    case Some(parcel) =>
      val updatedParcel = parcelUpdate.merge(parcel)
      db.run(parcel_query.filter(_.id === id).filter(_.original_department_id === department).update(updatedParcel)).map(_ => Some(updatedParcel))
    case None => Future.successful(None)
  }

  def deleteParcel(id: Long): Future[Int] = db.run(parcel_query.filter(_.id === id).delete)
}
