package pl.pai.restapi

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import pl.pai.restapi.http.HttpService
import pl.pai.restapi.services._
import pl.pai.restapi.utils.{Config, DatabaseService, FlywayService}

import scala.concurrent.ExecutionContext

object Main extends App with Config {
  implicit val actorSystem = ActorSystem()
  implicit val executor: ExecutionContext = actorSystem.dispatcher
  implicit val log: LoggingAdapter = Logging(actorSystem, getClass)
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val flywayService = new FlywayService(jdbcUrl, dbUser, dbPassword)
  flywayService.dropDatabase
  flywayService.migrateDatabaseSchema

  val databaseService = new DatabaseService(jdbcUrl, dbUser, dbPassword)

  val usersService = new PostmenService(databaseService)
  val authService = new AuthService(databaseService)(usersService)
  val actionEntryService = new ActionEntryService(databaseService)
  val departmentService = new DepartmentService(databaseService)
  val parcelService = new ParcelService(databaseService)

  val httpService = new HttpService(usersService, authService, actionEntryService, departmentService, parcelService)

  Http().bindAndHandle(httpService.routes, httpHost, httpPort)
}
