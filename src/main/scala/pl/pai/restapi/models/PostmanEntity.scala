package pl.pai.restapi.models

case class PostmanEntity(id: Option[Long] = None, username: String, password: String, name: Option[String] = None) {
  require(!username.isEmpty, "username.empty")
  require(!password.isEmpty, "password.empty")
}

case class PostmanEntityUpdate(username: Option[String] = None, password: Option[String] = None, name: Option[String] = None) {
  def merge(user: PostmanEntity): PostmanEntity = {
    PostmanEntity(user.id, username.getOrElse(user.username), password.getOrElse(user.password), user.name)
  }
}