package pl.pai.restapi.models

/**
  * Created by olek on 18.06.2017.
  */
case class ActionEntryEntity(id: Option[Long] = None, postman_id: Long, parcel_id: Long, department_id: Int, action_no: Int) {
  require(postman_id.isValidLong, "postman_id.undefined")
  require(parcel_id.isValidLong, "parcel_id.undefined")
  require(action_no.isValidLong, "action_no.undefined")
}
