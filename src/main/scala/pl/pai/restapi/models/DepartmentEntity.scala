package pl.pai.restapi.models

/**
  * Created by olek on 18.06.2017.
  */
case class DepartmentEntity(id: Option[Long] = None, region: String, web_address: String) {
  require(!region.isEmpty, "region.empty")
  require(!web_address.isEmpty, "web_address.empty")
}

case class DepartmentEntityUpdate(region: Option[String] = None, web_address: Option[String] = None) {
  def merge(department: DepartmentEntity): DepartmentEntity = {
    DepartmentEntity(department.id, region.getOrElse(department.region), web_address.getOrElse(department.web_address))
  }
}