package pl.pai.restapi.models

import akka.http.scaladsl.model.headers.Date

/**
  * Created by olek on 18.06.2017.
  */
case class ParcelEntity(id: Option[Long] = None, original_department_id: Long, receiver_address: String, weight: Double, last_modification_date: Int, sync: Boolean = false) {
  require(!receiver_address.isEmpty, "receiver_address.empty")
}

case class ParcelEntityUpdate(original_department_id: Option[Long], receiver_address: Option[String] = None, weight: Option[Double] = None, last_modification_date: Option[Int], sync: Option[Boolean]) {
  def merge(parcel: ParcelEntity): ParcelEntity = {
    ParcelEntity(parcel.id, original_department_id.getOrElse(parcel.original_department_id), receiver_address.getOrElse(parcel.receiver_address), weight.getOrElse(parcel.weight), last_modification_date.getOrElse(parcel.last_modification_date), sync.getOrElse(parcel.sync))
  }
}