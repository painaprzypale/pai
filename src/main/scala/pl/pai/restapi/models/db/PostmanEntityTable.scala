package pl.pai.restapi.models.db

import pl.pai.restapi.models.PostmanEntity
import pl.pai.restapi.utils.DatabaseService

trait PostmanEntityTable {

  protected val databaseService: DatabaseService
  import databaseService.driver.api._

  class Users(tag: Tag) extends Table[PostmanEntity](tag, "users") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def username = column[String]("username")
    def password = column[String]("password")
    def name = column[Option[String]]("name")

    def * = (id, username, password, name) <> ((PostmanEntity.apply _).tupled, PostmanEntity.unapply)
  }

  protected val users = TableQuery[Users]

}
