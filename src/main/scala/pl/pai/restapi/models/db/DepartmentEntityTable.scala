package pl.pai.restapi.models.db

import pl.pai.restapi.models.DepartmentEntity
import pl.pai.restapi.utils.DatabaseService

/**
  * Created by olek on 18.06.2017.
  */
trait DepartmentEntityTable {

  protected val databaseService: DatabaseService
  import databaseService.driver.api._

  class Departments(tag: Tag) extends Table[DepartmentEntity](tag, "department") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def region = column[String]("region")
    def web_address = column[String]("web_address")

    def * = (id, region, web_address) <> ((DepartmentEntity.apply _).tupled, DepartmentEntity.unapply)
  }

  protected val departments = TableQuery[Departments]
}
