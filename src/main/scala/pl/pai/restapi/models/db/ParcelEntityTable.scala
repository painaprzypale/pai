package pl.pai.restapi.models.db

import pl.pai.restapi.models.{ParcelEntity}
import pl.pai.restapi.utils.DatabaseService

/**
  * Created by olek on 18.06.2017.
  */
trait ParcelEntityTable {

  protected val databaseService: DatabaseService
  import databaseService.driver.api._

  class Parcel(tag: Tag) extends Table[ParcelEntity](tag, "parcel") {
    def id = column[Option[Long]]("id", O.AutoInc)
    def original_department_id = column[Long]("original_department_id")
    def receiver_address = column[String]("receiver_address")
    def weight = column[Double]("weight")
    def last_modification_date = column[Int]("last_modification_date")
    def pk = primaryKey("pk_a", (id, original_department_id))
    def sync = column[Boolean]("sync")
    def * = (id, original_department_id, receiver_address, weight, last_modification_date, sync) <> ((ParcelEntity.apply _).tupled, ParcelEntity.unapply)
  }

  protected val parcel_query = TableQuery[Parcel]
}
