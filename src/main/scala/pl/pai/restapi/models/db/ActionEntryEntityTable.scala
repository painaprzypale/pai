package pl.pai.restapi.models.db

import pl.pai.restapi.models.ActionEntryEntity
import pl.pai.restapi.utils.DatabaseService

/**
  * Created by olek on 18.06.2017.
  */
trait ActionEntryEntityTable {

  protected val databaseService: DatabaseService
  import databaseService.driver.api._

  class ActionEntries(tag: Tag) extends Table[ActionEntryEntity](tag, "action_entry") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def postman_id = column[Long]("postman_id")
    def parcel_id = column[Long]("parcel_id")
    def department_id = column[Int]("department_id")
    def action_no = column[Int]("action_no")

    def * = (id, postman_id, parcel_id, department_id, action_no) <> ((ActionEntryEntity.apply _).tupled, ActionEntryEntity.unapply)
  }

  protected val action_entries = TableQuery[ActionEntries]

}
