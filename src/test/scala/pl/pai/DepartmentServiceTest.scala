package pl.pai

import akka.http.scaladsl.model.{HttpEntity, MediaTypes}
import org.scalatest.concurrent.ScalaFutures
import pl.pai.restapi.models.DepartmentEntity
import io.circe.generic.auto._

import scala.util.Random

class DepartmentServiceTest  extends BaseServiceTest with ScalaFutures {

  import departmentService._

  trait Context {
    val testDepartment = provisionDepartmentList(10)
    val route = httpService.departmentRouter.route
  }

  "Department service" should {

    "retrieve department list" in new Context {
      Get("/department") ~> route ~> check {
        responseAs[Seq[DepartmentEntity]].isEmpty should be(false)
      }
    }

    "retrieve department by id" in new Context {
      val test = testDepartment(7)
      Get(s"/department/${test.id.get}") ~> route ~> check {
        responseAs[DepartmentEntity] should be(test)
      }
    }

    "update department by id and retrieve it" in new Context {
      val test = testDepartment(8)
      val newRegion = Random.nextString(10)
      val requestEntity = HttpEntity(MediaTypes.`application/json`, s"""{"region": "$newRegion"}""")

      Post(s"/department/${test.id.get}", requestEntity) ~> route ~> check {
        whenReady(getDepartmentById(test.id.get)) { result =>
          result.get.region should be(newRegion)
        }
      }
    }

  }
}
