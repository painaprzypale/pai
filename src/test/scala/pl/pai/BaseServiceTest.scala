package pl.pai

import akka.http.scaladsl.testkit.ScalatestRouteTest
import de.heikoseeberger.akkahttpcirce.CirceSupport
import org.scalatest._
import pl.pai.restapi.http.HttpService
import pl.pai.restapi.models.{ActionEntryEntity, DepartmentEntity, ParcelEntity, PostmanEntity}
import pl.pai.restapi.services._
import pl.pai.restapi.utils.DatabaseService

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.Random
import pl.pai.utils.InMemoryPostgresStorage._

trait BaseServiceTest extends WordSpec with Matchers with ScalatestRouteTest with CirceSupport {

  dbProcess.getProcessId

  private val databaseService = new DatabaseService(jdbcUrl, dbUser, dbPassword)

  val actionEntityService = new ActionEntryService(databaseService)
  val usersService = new PostmenService(databaseService)
  val authService = new AuthService(databaseService)(usersService)
  val actionEntryService = new ActionEntryService(databaseService)
  val departmentService = new DepartmentService(databaseService)
  val parcelService = new ParcelService(databaseService)

  val httpService = new HttpService(usersService, authService, actionEntryService, departmentService, parcelService)

  def provisionUsersList(size: Int): Seq[PostmanEntity] = {
    val savedUsers = (1 to size).map { _ =>
      PostmanEntity(Some(Random.nextLong()), Random.nextString(10), Random.nextString(10))
    }.map(usersService.createUser)

    Await.result(Future.sequence(savedUsers), 10.seconds)
  }

  def provisionTokensForUsers(usersList: Seq[PostmanEntity]) = {
    val savedTokens = usersList.map(authService.createToken)
    Await.result(Future.sequence(savedTokens), 10.seconds)
  }

  def provisionParcelList(size: Int): Seq[ParcelEntity] = {
    val savedParcel = (1 to size).map { _ =>
      ParcelEntity(Some(Random.nextLong()), Random.nextLong(), Random.nextString(10), Random.nextDouble(), Random.nextInt())
    }.map(parcelService.createParcel)

    Await.result(Future.sequence(savedParcel), 10.seconds)
  }

  def provisionDepartmentList(size: Int): Seq[DepartmentEntity] = {
    val savedDepartment = (1 to size).map { _ =>
      DepartmentEntity(Some(Random.nextInt()), Random.nextString(10), Random.nextString(10))
    }.map(departmentService.createDepartment)

    Await.result(Future.sequence(savedDepartment), 10.seconds)
  }

  def provisionActionEntryList(size: Int): Seq[ActionEntryEntity] = {
    val savedActionEntry = (1 to size).map { _ =>
      ActionEntryEntity(Some(Random.nextLong()), Random.nextLong(), Random.nextLong(), Random.nextInt(), Random.nextInt())
    }.map(actionEntityService.createActionEntry)

    Await.result(Future.sequence(savedActionEntry), 10.seconds)
  }

}
