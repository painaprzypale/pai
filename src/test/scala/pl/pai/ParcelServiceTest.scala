package pl.pai

import scala.util.Random
import akka.http.scaladsl.model.{HttpEntity, MediaTypes}
import io.circe.generic.auto._
import org.scalatest.concurrent.ScalaFutures
import pl.pai.restapi.models.ParcelEntity

class ParcelServiceTest extends BaseServiceTest with ScalaFutures {

  import parcelService._

  trait Context {
    val testParcel = provisionParcelList(10)
    val route = httpService.parcelRouter.route
  }

  "Parcel service" should {

    "retrieve parcel list" in new Context {
      Get("/parcel") ~> route ~> check {
        responseAs[Seq[ParcelEntity]].isEmpty should be(false)
      }
    }

//    "retrieve parcel by id" in new Context {
//      val test = testParcel(7)
//      Get(s"/parcel/${test.id.get}") ~> route ~> check {
//        responseAs[ParcelEntity] should be(test)
//      }
//    }

//    "update parcel by id and retrieve it" in new Context {
//      val test = testParcel(8)
//      val newAddress = Random.nextString(10)
//      val requestEntity = HttpEntity(MediaTypes.`application/json`, s"""{"receiver_address": "$newAddress"}""")
//
//      Post(s"/parcel/${test.id.get}", requestEntity) ~> route ~> check {
//        whenReady(getParcelById(test.id.get)) { result =>
//          result.get.receiver_address should be(newAddress)
//        }
//      }
//    }

  }
}
