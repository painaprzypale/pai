package pl.pai

import org.scalatest.concurrent.ScalaFutures
import pl.pai.restapi.models.ActionEntryEntity
import io.circe.generic.auto._

/**
  * Created by olek on 19.06.2017.
  */
class ActionEntryTest extends BaseServiceTest with ScalaFutures {

  trait Context {
    val testActionEntry = provisionActionEntryList(10)
    val route = httpService.actionEntryRouter.route
  }

  "ActionEntry service" should {

    "retrieve actionEntry list" in new Context {
      Get("/action_entry") ~> route ~> check {
        responseAs[Seq[ActionEntryEntity]].isEmpty should be(false)
      }
    }

    "retrieve actionEntry by id" in new Context {
      val test = testActionEntry(2)
      Get(s"/action_entry/${test.id.get}") ~> route ~> check {
        responseAs[ActionEntryEntity] should be(test)
      }
    }
  }

}
