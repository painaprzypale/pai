<?php
    require_once('config.php');
    require_once('statuses.php');
    
    function send_rest_request($request, $url) {
        global $CONFIG;
    
        $url = $CONFIG['BACKEND_ROOT'] . $url;
        
        $json_query = json_encode($request);
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $json_query);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        $result = curl_exec($curl);
        
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        
        $response = compact('status');
        
        if ($result != 'null') {
            $response['body'] = json_decode($result);
        }
        
        return $response;
    }
    
    function send_get_request($url) {
        global $CONFIG;
    
        $url = $CONFIG['BACKEND_ROOT'] . $url;
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        $result = curl_exec($curl);
        
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        
        $response = compact('status');
        
        if ($result != 'null') {
            $response['body'] = json_decode($result);
        }
        
        return $response;
    }
    
    $HTTP_CODES = [
        'OK' => 200];
    
    function redirect($url) {
        global $CONFIG;
    
        echo '<script>window.location = "' . $CONFIG['FRONTEND_ROOT'] . $url . '";</script>';
        exit(0);
    }
    
    function redirect_to_dashboard() {
        global $CONFIG;
        redirect($CONFIG['DASHBOARD_URL']);
    }
    
    function redirect_to_sign_in() {
        global $CONFIG;
        redirect($CONFIG['SIGN_IN_URL']);
    }
    
    function redirect_to_service_down() {
        global $CONFIG;
        redirect($CONFIG['SERVICE_DOWN_URL']);
    }
    
    function response_ok($response) {
        global $HTTP_CODES;
        if ($response['status'] == 0) {
            redirect_to_service_down();
        }
        return $response['status'] == $HTTP_CODES['OK'] && isset($response['body']);
    }
    
    function login($login, $password, $remember) {
        global $CONFIG;
        global $HTTP_CODES;
        
        $request = compact('login', 'password');
        $response = send_rest_request($request, 'auth/signIn');
        
        
        if (response_ok($response)) {
            global $CONFIG;
            
            if ($remember == true) {
                $time = time() + 60*60*24*365*5; // Five years should be enough
            } else {
                $time = 0;  // Which means - to the end of session.
            }
        
            setcookie ($CONFIG['COOKIE_NAME'], $response['body']->token, $time, '/');
            redirect_to_dashboard();
        }
    }
    
    function logout() {
        global $CONFIG;
        setcookie ($CONFIG['COOKIE_NAME'], '', time()-3600, '/');
    }
    
    function get_user_id() {
        global $CONFIG;
    
        if(!isset($_COOKIE[$CONFIG['COOKIE_NAME']])) {
            return -1;
        }
        
        $token = $_COOKIE[$CONFIG['COOKIE_NAME']];
        $response = send_rest_request($token, 'auth/authenticate');
        
        if(response_ok($response)) {
            return $response['body']->id;
        } else {
            return -1;
        }
    }
    
    function require_logout() {
        $user_id = get_user_id();
        if ($user_id != -1) {
            redirect_to_dashboard();
        }
    }
    
    function require_login() {
        $user_id = get_user_id();
        if ($user_id == -1) {
            redirect_to_sign_in();
        }
    }
    
    function translate_status($s) {
        global $STATUS;
        
        if (isset($STATUS[$s])) {
            return $STATUS[$s];
        } else {
            return $STATUS['default'];
        }
    }
    
    function get_parcel_status($parcel_id) {
        $response = send_get_request('action_entry/of/' . $parcel_id);
        if (response_ok($response)) {
            return $response['body'];
        } else {
            return null;
        }
    }
    
    function change_parcel_status($parcel_id, $action_no, $department_id) {
        $postman_id = get_user_id();
        $request = compact('parcel_id', 'postman_id', 'department_id', 'action_no');
        
        $response = send_rest_request($request, 'action_entry');
        if (!response_ok($response)) {
            return $response;
        }
        return true;
    }
    
    function get_department_name($department_id) {
        $response = send_get_request('department/' . $department_id);
        if (response_ok($response)) {
            return $response['body']->region;
        } else {
            return null;
        }
    }
?>