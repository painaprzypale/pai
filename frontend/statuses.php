<?php
    
    $STATUS_NUM = [
        'none' => 0,
        'in_a_storehouse' => 1,
        'on_a_way' => 2,
        'delivered' => 3
    ];
    
    $STATUS = [
        'default' => 'Zagubiona',
        $STATUS_NUM['none'] => '<i>Nieznany</i>',
        $STATUS_NUM['in_a_storehouse'] => 'W magazynie',
        $STATUS_NUM['on_a_way'] => 'W drodze',
        $STATUS_NUM['delivered'] => 'Dostarczona'
    ];
?>