<?php
    require_once 'middleend.php';
    
    require_logout();
    
    if (isset($_POST['submit'])) {
        $remember = ((isset($_POST['remember'])) && ($_POST['remember'] == 'yes')) ? true : false;
        login($_POST['login'], $_POST['passwd'], $remember);
    }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Logowanie</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
  
  <?php
    if (isset($_POST['submit'])) {
  ?>
        <div class="alert alert-danger">
            <strong>Błąd!</strong> Nieprawidłowy login lub hasło.
        </div>
        
    <?php
    }
    ?>

    <div class="container">

      <form class="form-signin" action="signin.php" method="post">
        <h2 class="form-signin-heading">Zaloguj się</h2>
        <label for="inputEmail" class="sr-only">Login</label>
        <input name="login" type="login" id="inputEmail" class="form-control" placeholder="Login" required autofocus>
        <label for="inputPassword" class="sr-only">Hasło</label>
        <input name="passwd" type="password" id="inputPassword" class="form-control" placeholder="Hasło" required>
        <div class="checkbox">
          <label>
            <input name='remember' type="checkbox" value="yes"> Zapamiętaj mnie
          </label>
        </div>
        <button name="submit" class="btn btn-lg btn-primary btn-block" type="submit">Zaloguj</button>
      </form>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>