<?php
    require_once('middleend.php');
    
    require_login();
    
    if(isset($_GET['id'])) {
        if (isset($_GET['status_to']) && $_GET['department_id']) {
            $result = change_parcel_status($_GET['id'], $_GET['status_to'], $_GET['department_id']);
            if ($result !== true) {
                // TODO Error.
            }
        }
    }
    
    
    $parcels = send_get_request('parcel/');
    
    if(!response_ok($parcels)) {
        echo 'error! Status: ' . $parcels['status'];
        die;
    }
    
    $parcels = $parcels['body'];
    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Przesyłki - lista</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?= $CONFIG['FRONTEND_ROOT'] ?>">Przesyłki</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="parcels.php">Przesyłki</a></li>
<!--             <li><a href="#">Settings</a></li> -->
<!--             <li><a href="#">Profile</a></li> -->
            <li><a href="signout.php">Wyloguj</a></li>
          </ul>
          <!--<form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>-->
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="parcels.php">Lista przesyłek <span class="sr-only">(current)</span></a></li>
            <!--<li><a href="#">Reports</a></li>
            <li><a href="#">Analytics</a></li>
            <li><a href="#">Export</a></li>-->
          </ul>
          <!--<ul class="nav nav-sidebar">
            <li><a href="">Nav item</a></li>
            <li><a href="">Nav item again</a></li>
            <li><a href="">One more nav</a></li>
            <li><a href="">Another nav item</a></li>
            <li><a href="">More navigation</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="">Nav item again</a></li>
            <li><a href="">One more nav</a></li>
            <li><a href="">Another nav item</a></li>
          </ul>-->
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Przesyłki</h1>

          <!--<div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
          </div>-->

<!--           <h2 class="sub-header">Section title</h2> -->
          <?php if (empty($parcels)) {
          ?><div class="alert alert-info">
            Brak przesyłek.
          </div>
          <?php
            } else {
          ?>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Kod</th>
                  <th>Lokalizacja</th>
                  <th>Adres dostawy</th>
                  <th>Waga</th>
                  <th>Status</th>
                  <th>Akcje</th>
                </tr>
              </thead>
              <tbody>
              <?php
                foreach ($parcels as $parcel) {
                    $parcel_status = get_parcel_status($parcel->id);
                    if(!is_null($parcel_status)) {
                        if($parcel_status->action_no == $STATUS_NUM['delivered']) {
                            continue;
                        }
                        $department_name = get_department_name($parcel_status->department_id);
                    }
              ?>
                <tr>
                  <td><?= $parcel->id; ?></td>
                  <td><?php 
                    if (isset($department_name)) {
                        echo $department_name;
                    } else {
                        echo '<i>Nieznana</i>';
                    }
                  ?></td>
                  <td><?= $parcel->receiver_address; ?></td>
                  <td><?php printf('%.2f', $parcel->weight); ?> kg</td>
                  <td><?= translate_status((!is_null($parcel_status)) ? $parcel_status->action_no : 'null'); ?></td>
                  <td>
                  <?php
                    if(isset($parcel_status->action_no)) {
                        switch($parcel_status->action_no) {
                            case $STATUS_NUM['in_a_storehouse']:
                                ?><a href="parcels.php?id=<?= $parcel->id; ?>&status_to=<?= $STATUS_NUM['on_a_way']; ?>&department_id=<?= $parcel_status->department_id; ?>">Weź</a><?php
                                break;
                            case $STATUS_NUM['on_a_way']:
                                ?><a href="parcels.php?id=<?= $parcel->id; ?>&status_to=<?= $STATUS_NUM['delivered']; ?>&department_id=<?= $parcel_status->department_id; ?>">Dostarcz</a>
                                &nbsp;<a href="pick_department.php?id=<?= $parcel->id; ?>&status_to=<?= $STATUS_NUM['in_a_storehouse']; ?>">Oddaj</a><?php
                                break;
                        }
                    }
                  ?>
                  </td>
                </tr>
               <?php
                }
               }
               ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<!--     <script src="assets/js/vendor/holder.min.js"></script> -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
