<?php
    $CONFIG = [
        'BACKEND_ROOT'  => 'http://127.0.0.1:9000/v1/',
        'FRONTEND_ROOT' => 'http://127.0.0.1/~mateusz/PAI/',
        'COOKIE_NAME'   => 'restapi_pai_pl',
        'DASHBOARD_URL' => 'parcels.php',
        'SIGN_IN_URL'   => 'signin.php',
        'SERVICE_DOWN_URL' => 'server-down.php'];
?>