<?php
    require_once 'middleend.php';
    
    require_login();
    logout();
    redirect_to_sign_in();
?>