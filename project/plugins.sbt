resolvers += Classpaths.sbtPluginReleases
resolvers ++= Seq(
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
)

addSbtPlugin("io.spray" %% "sbt-revolver" % "0.8.0")
addSbtPlugin("com.typesafe.sbt" %% "sbt-native-packager" % "1.1.4")
addSbtPlugin("io.gatling" % "sbt-plugin" % "1.0-RC5")